**Opis**

Front-endowa warstwa aplikacji do przeszukiwania kart z gry Hearthstone która, znajduje się pod tym adresem: <https://playhearthstone.com/pl-pl/>

---

## Wymagania

Potrzebne programy i pakiety

1. NodeJS - <https://nodejs.org/en/>
2. JSON Server - <https://github.com/typicode/json-server>

---

## Uruchomienie

1. W konsoli odpalić polecenie `npm install` w katalogu root z repozytorium.
2. Wprowadzić komendę w konsoli `npm start` w folderze root.
3. W nowym oknie terminala przejdź do folderu `./db/`.
4. Wprowadz komendę `json-server --watch db.json`.

Po chwili na lokalnym adresie http://localhost:3000 powinna pokazać sie aplikacja.

---

## Korzystałem z następujących technologii

1. ReactJS i biblioteki:
  - redux
  - react-router-dom
  - react-redux
  - redux-logger
  - redux-thunk
2. Axios
3. Bootstrap 4

Lista Kart oraz obrazki są pobierane z API <https://hearthstonejson.com/>