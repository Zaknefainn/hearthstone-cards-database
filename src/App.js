import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link, NavLink, Switch} from 'react-router-dom';
import MainPage from './containers/main-page';
import CardsDatabase from './containers/cards-database';
import NoPageFound from './components/no-page-found';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <nav>
              <ul>
                <li><NavLink to='/' activeClassName='active-nav'>Home</NavLink></li>
                <li><NavLink to='/cards' activeClassName='active-nav'>All Cards</NavLink></li>
              </ul>
            </nav>
          </header>
          <section className="container-fluid">
            <Switch>
              <Route exact path='/' component={MainPage}></Route>
              <Route path='/cards' component={CardsDatabase}></Route>
              <Route component={NoPageFound}></Route>
            </Switch>
          </section>
          <footer className="container-fluid">
            stopka
          </footer>
        </div>
      </Router>
    );
  }
}

export default App;
