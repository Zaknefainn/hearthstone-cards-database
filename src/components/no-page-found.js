import React from 'react';

const NoPageFound = (props) => {
    return (
        <div>
            <h2>Strony nie znaleźiono</h2>
            <h3>404 ERROR</h3>
        </div>
    );
}

export default NoPageFound;

