import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// import logger from 'redux-logger';
import { createLogger } from 'redux-logger'

const logger = createLogger({
  });

const store = createStore(
  reducers,
  applyMiddleware(thunk)
  // applyMiddleware(logger, thunk)
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();