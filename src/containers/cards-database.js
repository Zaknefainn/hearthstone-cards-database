import React, {Component} from 'react';
import ClassList from './class-list';
import CardList from './cards-list';
import SearchCards from './search-cards';
import CardFilter from './cards-filter';

export default class extends Component {
    constructor(props){
        super(props);

        this.state = {
            classSelected: "PALADIN" //default class
        }
    }

    setClassSelected(value) {
        this.setState({classSelected: value});
    }

    render() { 
        return(
            <div>  
                <h2>Hearthstone Cards by class</h2>
                <div className="class-list">
                    <ClassList setClassSelected={this.setClassSelected.bind(this)} currentClass={this.state.classSelected}></ClassList>
                    <CardFilter></CardFilter>
                </div>
                <div className="search-card">
                    <SearchCards></SearchCards>
                </div>
                <div className="cards-list">
                    <CardList currentClass={this.state.classSelected}></CardList>
                </div>
            </div>
        );
    }
}