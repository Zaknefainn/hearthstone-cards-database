import React, {Component} from 'react';

export default class extends Component{
    constructor(props){
        super(props);
    }

    // TODO: może wyodrębnic te tabelki jako komponent
    render(){
        return(
            <div>
                <h2>hall of fame</h2>
                <div className="row">
                    <div className="col-md-8">
                        <div className="row">
                            <div className="col-md-6">
                                <h3>Best Community Card</h3>
                                <ul>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                </ul>
                            </div>
                            <div className="col-md-6">
                                <h3>Top Useless Card</h3>
                                <ul>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <h3>Hall of Fame</h3>
                                <ul>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                </ul>
                            </div>
                            <div className="col-md-6">
                                <h3>Community Creations</h3>
                                <ul>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                    <li>card</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <h3>Top Standard Card</h3>
                        <ul>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                            <li>card</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}


