import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions';

class CardsFilter extends Component {
  constructor(props) {
    super(props);
  }

  handleFilterChange(e) {
    let newFilterSettings = this.props.filterSelected;
    
    if(e.target.id === "card-type") {

      newFilterSettings.typeSelected = e.target.value;

    } else if(e.target.id === "card-rarity") {

      newFilterSettings.raritySelected = e.target.value;

    } else if(e.target.id === "card-cost") {

      newFilterSettings.costSelected = e.target.value;

    } else if (e.target.id === "card-mechanics") {

      newFilterSettings.mechanicsSelected = e.target.value;

    } else if (e.target.id === "set-exp") {

      newFilterSettings.setSelected = e.target.value;

    } else if (e.target.id === "race-cards") {

      newFilterSettings.raceSelected = e.target.value;
    }

    this.props.filtersSelectedAction(newFilterSettings);
  }

  render() {
    return(
      <div>
        <h4>Filtr</h4>
        <form action="#">
          <div className="row">
            <div className="col-md-4">
              <div className="form-group">
                <label htmlFor="card-type">Type</label>
                <select onChange={this.handleFilterChange.bind(this)} value={this.props.filterSelected.typeSelected} className="form-control" name="card-type" id="card-type">
                  <option value="ALL">All</option>
                  <option value="SPELL">Spell</option>
                  <option value="MINION">Minion</option>
                  <option value="ENCHANTMENT">Enchantment</option>
                  <option value="WEAPON">Weapon</option>
                  <option value="HERO">Hero</option>
                  <option value="HERO_POWER">Hero power</option>
                </select>
              </div>
            </div>
            <div className="col-md-4">
              <div className="form-group">
                <label htmlFor="card-rarity">Rarity</label>
                <select onChange={this.handleFilterChange.bind(this)} value={this.props.filterSelected.raritySelected} className="form-control" name="card-rarity" id="card-rarity">
                  <option value="ALL">All</option>
                  <option value="LEGENDARY">Legendary</option>
                  <option value="EPIC">Epic</option>
                  <option value="RARE">Rare</option>
                  <option value="COMMON">Common</option>
                </select>
              </div>
            </div>
            <div className="col-md-4">
              <div className="form-group">
                <label htmlFor="card-mechanics">Mechanics</label>
                <select onChange={this.handleFilterChange.bind(this)} value={this.props.filterSelected.mechanicsSelected} className="form-control" name="card-mechanics" id="card-mechanics">
                  <option value="ALL">All</option>
                  <option value="TOPDECK">TOPDECK</option>
                  <option value="DEATHRATTLE">DEATHRATTLE</option>
                  <option value="CHOOSE_ONE">CHOOSE_ONE</option>
                  <option value="BATTLECRY">BATTLECRY</option>
                  <option value="INSPIRE">INSPIRE</option>
                  <option value="TAG_ONE_TURN_EFFECT">TAG_ONE_TURN_EFFECT</option>
                  <option value="AURA">AURA</option>
                  <option value="FORGETFUL">FORGETFUL</option>
                  <option value="TRIGGER_VISUAL">TRIGGER_VISUAL</option>
                  <option value="SPELLPOWER">SPELLPOWER</option>
                  <option value="DIVINE_SHIELD">DIVINE_SHIELD</option>
                  <option value="STEALTH">STEALTH</option>
                  <option value="TAUNT">TAUNT</option>
                  <option value="CANT_ATTACK">CANT_ATTACK</option>
                  <option value="TRIGGER_VISUAL">TRIGGER_VISUAL</option>
                  <option value="SECRET">SECRET</option>
                  <option value="AFFECTED_BY_SPELL_POWER">AFFECTED_BY_SPELL_POWER</option>
                  <option value="AI_MUST_PLAY">AI_MUST_PLAY</option>
                  <option value="InvisibleDeathrattle">InvisibleDeathrattle</option>
                  <option value="ImmuneToSpellpower">ImmuneToSpellpower</option>
                  <option value="EVIL_GLOW">EVIL_GLOW</option>
                  <option value="WINDFURY">WINDFURY</option>
                  <option value="CHARGE">CHARGE</option>
                  <option value="GRIMY_GOONS">GRIMY_GOONS</option>
                  <option value="KABAL">KABAL</option>
                  <option value="JADE_GOLEM">JADE_GOLEM</option>
                  <option value="JADE_LOTUS">JADE_LOTUS</option>
                  <option value="LIFESTEAL">LIFESTEAL</option>
                  <option value="COMBO">COMBO</option>
                  <option value="FREEZE">FREEZE</option>
                </select>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="form-group">
                <label htmlFor="set-exp">Addition</label>
                <select onChange={this.handleFilterChange.bind(this)} value={this.props.filterSelected.setSelected} className="form-control" name="set-exp" id="set-exp">
                  <option value="ALL">All</option>
                  <option value="CORE">Core</option>
                  <option value="EXPERT1">Expert</option>
                  <option value="NAXX">Curse of Naxxramas</option>
                  <option value="GVG">Goblins vs Gnomes</option>
                  <option value="BRM">Blackrock Mountain</option>
                  <option value="TGT">The Grand Tournament</option>
                  <option value="LOE">League of Explorers</option>
                  <option value="OG">Whispers of the Old Gods</option>
                  <option value="GANGS">Mean Streets of Gadgetzan</option>
                  <option value="UNGORO">Journey to Un'Goro</option>
                  <option value="ICECROWN">Knights of the Frozen Throne</option>
                  <option value="LOOTAPALOOZA">Kobolds and Catacombs</option>
                  <option value="GILNEAS">The Witchwood</option>
                </select>
              </div>
            </div>

            <div className="col-md-4">
              <div className="form-group">
                <label htmlFor="race-cards">Race</label>
                <select onChange={this.handleFilterChange.bind(this)} value={this.props.filterSelected.raceSelected} className="form-control" name="race-cards" id="race-cards">
                  <option value="ALL">All</option>
                  <option value="BEAST">BEAST</option>
                  <option value="MURLOC">MURLOC</option>
                  <option value="ELEMENTAL">ELEMENTAL</option>
                  <option value="TOTEM">TOTEM</option>
                  <option value="DEMON">DEMON</option>
                  <option value="DRAGON">DRAGON</option>
                  <option value="PIRATE">PIRATE</option>
                  <option value="MECHANICAL">MECHANICAL</option>
                </select>
              </div>
            </div>

            <div className="col-md-4">
              <div className="form-group">
                <label htmlFor="card-cost">Cost</label>
                <select onChange={this.handleFilterChange.bind(this)} value={this.props.filterSelected.costSelected} className="form-control" name="card-cost" id="card-cost">
                  <option value="ALL">All</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10+</option>
                </select>
              </div>
            </div>
            
          </div>
        </form>
        <h4>Sort</h4>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    filterSelected: state.filterSelected
  }
} 

const mapDispatchToProps = (dispatch) => {
  return {
    getListCards: bindActionCreators(actions.getListCards, dispatch),
    filtersSelectedAction: bindActionCreators(actions.filtersSelectedAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CardsFilter);