import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions';
var find = require('lodash.find');

class Card extends Component {
  constructor(props) {
    super(props);

    this.state = {
      style: {
        display: 'block',
      },
      filters: {
        type: true,
        rarity: true,
        mechanics: true,
        cost: true,
        set: true,
        race: true
      }
    }
  }

  componentWillMount(){
    this.hiddeUslesCards();
  }

  componentWillReceiveProps() {

    this.filterInit();
  }

  filterInit() {
    if (this.props.filterSetting.typeSelected !== 'ALL') {

     this.typeSelected();

    } else if (this.props.filterSetting.typeSelected === 'ALL') {

        this.setTypeFilterState(true);
    }

    if (this.props.filterSetting.raritySelected !== 'ALL') {

      this.raritySelected();

    } else if (this.props.filterSetting.raritySelected === 'ALL') {

      this.setRarityFilterState(true);
    }

    if (this.props.filterSetting.costSelected !== 'ALL') {

      this.costSelected();

    } else if (this.props.filterSetting.costSelected === 'ALL') {

      this.setCostFilterState(true);
    }

    if (this.props.filterSetting.mechanicsSelected !== 'ALL') {

      this.mechanicsSelected();

    } else if (this.props.filterSetting.costSelected === 'ALL') {

      this.setMechanicsFilterState(true);
    }

    if (this.props.filterSetting.setSelected !== 'ALL') {

      this.setSelected();

    } else if (this.props.filterSetting.setSelected === 'ALL') {

      this.setSetFilterState(true);
    }

    if (this.props.filterSetting.raceSelected !== 'ALL') {

      this.raceSelected();

    } else if (this.props.filterSetting.raceSelected === 'ALL') {

      this.setRaceFilterState(true);
    }
    
    this.showOrHiddeCard();
  }

  showOrHiddeCard() {
    if(this.isDisplayedCard()) {

      this.setState({style: {display: 'block'}});

    } else if (!this.isDisplayedCard()) {

      this.setState({style: {display: 'none'}});
    }
  }

  hiddeUslesCards() {
    if (this.props.card.collectible === undefined || this.props.card.set === 'HERO_SKINS' || this.props.card.set === 'CORE') {

      this.setState({type: {display: 'none'}});
      
    } else {
      
      this.setState({style: {display: 'block'}});
    }
  }

  typeSelected() {

      if (this.props.filterSetting.typeSelected !== this.props.card.type) {

        this.setTypeFilterState(false);

      }  else if (this.props.filterSetting.typeSelected === this.props.card.type) {

        this.setTypeFilterState(true);
      }
  }

  setTypeFilterState(status = true) {
    const object = Object.assign({},
      this.state.filters,
      this.state.filters.type = status
    );
    this.setState({filters: object});
  }

  raritySelected() {

      if (this.props.filterSetting.raritySelected !== this.props.card.rarity) {

        this.setRarityFilterState(false);
        
      } else if (this.props.filterSetting.raritySelected === this.props.card.rarity) {

        this.setRarityFilterState(true);
      }
  }

  setRarityFilterState(status = true) {
    const object = Object.assign({},
      this.state.filters,
      this.state.filters.rarity = status
    );
    this.setState({filters: object});
  }

  costSelected() {

      if (this.props.filterSetting.costSelected != this.props.card.cost) {

        this.setCostFilterState(false);

      } else if (this.props.filterSetting.costSelected == this.props.card.cost) {

        this.setCostFilterState(true);
      }
  }

  setCostFilterState(status = true) {
    const object = Object.assign({},
      this.state.filters,
      this.state.filters.cost = status
    );
    this.setState({filters: object});
  }

  mechanicsSelected() {
    let isAvalible = false;
    
    //hide if no contain mechanics
    if (!this.props.card.mechanics) {
      this.setMechanicsFilterState(false);
      return;
    }

    for (let index = 0; index < this.props.card.mechanics.length; index++) {

      if (this.props.card.mechanics[index] === this.props.filterSetting.mechanicsSelected) {

        isAvalible = true;
      }
    }

    if (!isAvalible) {

      this.setMechanicsFilterState(false);

    } else if (isAvalible) {

      this.setMechanicsFilterState(true);
    }
  }

  setMechanicsFilterState(status = true) {
    const object = Object.assign({},
      this.state.filters,
      this.state.filters.mechanics = status
    );
    this.setState({filters: object});
  }

  setSelected() {

      if (this.props.filterSetting.setSelected != this.props.card.set) {

        this.setSetFilterState(false);

      } else if (this.props.filterSetting.setSelected == this.props.card.set) {

        this.setSetFilterState(true);
      }
  }

  setSetFilterState(status = true) {
    const object = Object.assign({},
      this.state.filters,
      this.state.filters.set = status
    );
    this.setState({filters: object});
  }

  raceSelected() {

      if (this.props.filterSetting.raceSelected != this.props.card.race) {

        this.setRaceFilterState(false);

      } else if (this.props.filterSetting.raceSelected == this.props.card.race) {

        this.setRaceFilterState(true);
      }
  }

  setRaceFilterState(status = true) {
    const object = Object.assign({},
      this.state.filters,
      this.state.filters.race = status
    );
    this.setState({filters: object});
  }

  isDisplayedCard() {
    let isDisplayed = false;

    if (this.state.filters.type && this.state.filters.rarity && this.state.filters.mechanics && this.state.filters.cost && this.state.filters.set && this.state.filters.race) {
      isDisplayed = true;
    }
  
    return isDisplayed;
  }

  render() {
    const url = `https://art.hearthstonejson.com/v1/render/latest/enUS/512x/${this.props.card.id}.png`;
    
    return (
      <div className="col-sm-3 card" style={this.state.style}>
        <img src={url} title={this.props.card.id} alt={this.props.card.name.enUS}/>
      </div>
    );
  }
} 

export default Card;