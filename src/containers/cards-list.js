import React, {Component} from 'react';
import Card from './card';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions';
import * as classAction from '../actions/class-cards.actions';

class CardsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardCounter: 0,
    }
  }

  componentDidMount() {
    this.props.getClassCards(this.props.currentClass);
  }

  renderCardList() {
    let cardsListasd;

    if (this.props.currentClass === "PALADIN") {

      cardsListasd = this.props.paladinCards;    

    } else if (this.props.currentClass === "DRUID") {

      cardsListasd = this.props.druidCards;

    } else if (this.props.currentClass === "HUNTER") {

      cardsListasd = this.props.hunterCards;

    } else if (this.props.currentClass === "MAGE") {

      cardsListasd = this.props.mageCards;

    } else if (this.props.currentClass === "PRIEST") {

      cardsListasd = this.props.priestCards;

    } else if (this.props.currentClass === "ROGUE") {

      cardsListasd = this.props.rogueCards;

    } else if (this.props.currentClass === "SHAMAN") {

      cardsListasd = this.props.shamanCards;

    } else if (this.props.currentClass === "WARLOCK") {

      cardsListasd = this.props.warlockCards;

    } else if (this.props.currentClass === "WARRIOR") {

      cardsListasd = this.props.warriorCards;

    } else if (this.props.currentClass === "NEUTRAL") {

      cardsListasd = this.props.narutalCards;

    }

    return (
      cardsListasd.map((card) => {
        return <Card key={card.id} filterSetting={this.props.filterSelected} card={card}></Card>
      })
    );
  }

  render() {
    return(
      <div>
        <h3>Card List: {this.state.cardCounter}</h3>
        {/* TODO: tu w tym komponencie dodac short card view */}
        <div className="row">
          {this.renderCardList()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cardList: state.cardList,
    paladinCards: state.paladinCards,
    druidCards: state.druidCards,
    hunterCards: state.hunterCards,
    mageCards: state.mageCards,
    priestCards: state.priestCards,
    rogueCards: state.rogueCards,
    shamanCards: state.shamanCards,
    warlockCards: state.warlockCards,
    warriorCards: state.warriorCards,
    narutalCards: state.narutalCards,
    cardCounter: state.cardCounter,
    filterSelected: state.filterSelected
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getClassCards: bindActionCreators(classAction.getClassCards, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CardsList);
