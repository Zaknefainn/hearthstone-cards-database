import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions';
import * as classAction from '../actions/class-cards.actions';

class ClassList extends Component {
  constructor(props){
    super(props);
  }

  componentDidMount() {
    this.props.getAllClass();
  }

  handleClassSwitch(e) {
    const id = e.currentTarget.id;
    let newFiltersParametrs = this.resetFilters();

    this.props.getClassCards(e.currentTarget.id);
    this.props.setClassSelected(id);

    console.log('newFiltersParametrs', newFiltersParametrs);
  
    this.props.filtersSelectedAction(newFiltersParametrs);
  }

  resetFilters() {
    let currentFilters = this.props.filterSelected;

    Object.keys(currentFilters).forEach(function(key, index) {
      console.log('key', key);
      console.log('index', index);
      
      if(key == 'classSelected') {

        currentFilters[key] = "PALADIN";

      } else {

        currentFilters[key] = 'ALL'
      }
    });

    // delete currentFilters["index"];

    return currentFilters;
  }

  renderList() {
    const classNames = this.props.heroClass;
    const currentClass = this.props.currentClass;

    return (
      <ul className="class-list">
        {classNames.map((hero) =>
          <li key={hero.name}>
          {hero.parametr === currentClass ? (
            <button className="btn btn-primary class-button active-class" type="button" id={hero.parametr.toString()} onClick={this.handleClassSwitch.bind(this)}>{hero.name}</button>  
          ) : (
            <button className="btn btn-primary class-button" type="button" id={hero.parametr.toString()} onClick={this.handleClassSwitch.bind(this)}>{hero.name}</button>  
          )}
          </li>
        )}
      </ul>
    );
  }

  render(){
    return(
      <div>
          {this.renderList()}    
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    heroClass: state.heroClass,
    filterSelected: state.filterSelected
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAllClass: bindActionCreators(actions.getAllClass, dispatch),
    filtersSelectedAction: bindActionCreators(actions.filtersSelectedAction, dispatch),
    getClassCards: bindActionCreators(classAction.getClassCards, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassList);