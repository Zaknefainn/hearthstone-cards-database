import axios from 'axios';
import {ajaxDataFailed} from './index';

export const SHAMAN_CLASS = 'SHAMAN_CLASS';
export const PALADIN_CLASS = 'PALADIN_CLASS';
export const DRUID_CLASS = 'DRUID_CLASS';
export const HUNTER_CLASS = 'HUNTER_CLASS';
export const MAGE_CLASS = 'MAGE_CLASS';
export const PRIEST_CLASS = 'PRIEST_CLASS';
export const ROGUE_CLASS = 'ROGUE_CLASS';
export const WARLOCK_CLASS = 'WARLOCK_CLASS';
export const WARRIOR_CLASS = 'WARRIOR_CLASS';
export const NEUTRAL_CLASS = 'NEUTRAL_CLASS';

export function paladinCardsRequest(response) {

  return {
    type: PALADIN_CLASS,
    payload: response.data
  }
}

export function druidCardsRequest(response) {

  return {
    type: DRUID_CLASS,
    payload: response.data
  }
}

export function hunterCardsRequest(response) {

  return {
    type: HUNTER_CLASS,
    payload: response.data
  }
}

export function mageCardsRequest(response) {

  return {
    type: MAGE_CLASS,
    payload: response.data
  }
}

export function pritestCardsRequest(response) {

  return {
    type: PRIEST_CLASS,
    payload: response.data
  }
}

export function rogueCardsRequest(response) {

  return {
    type: ROGUE_CLASS,
    payload: response.data
  }
}

export function shamanCardsRequest(response) {

  return {
    type: SHAMAN_CLASS,
    payload: response.data
  }
}

export function warlockCardsRequest(response) {

  return {
    type: WARLOCK_CLASS,
    payload: response.data
  }
}

export function warriorCardsRequest(response) {

  return {
    type: WARRIOR_CLASS,
    payload: response.data
  }
}

export function naturalCardsRequest(response) {

  return {
    type: NEUTRAL_CLASS,
    payload: response.data
  }
}

export const getClassCards = (whatClass = "SHAMAN") => {

  return (dispatch) => {

    // axios.get(`http://localhost:3004/cards?cardClass=${whatClass}&_limit=10`)
    axios.get(`http://localhost:3004/cards?cardClass=${whatClass}`)
      .then((response) => {
        
        if(whatClass === 'SHAMAN') {

          dispatch(shamanCardsRequest(response));

        } else if(whatClass === 'WARLOCK') {

          dispatch(warlockCardsRequest(response));

        } else if(whatClass === 'PALADIN') {

          dispatch(paladinCardsRequest(response));

        } else if(whatClass === 'DRUID') {

          dispatch(druidCardsRequest(response));

        } else if(whatClass === 'HUNTER') {

          dispatch(hunterCardsRequest(response));

        } else if(whatClass === 'MAGE') {

          dispatch(mageCardsRequest(response));

        } else if(whatClass === 'PRIEST') {

          dispatch(pritestCardsRequest(response));

        } else if(whatClass === 'ROGUE') {

          dispatch(rogueCardsRequest(response));

        } else if(whatClass === 'WARRIOR') {

          dispatch(warriorCardsRequest(response));

        } else if(whatClass === 'NEUTRAL') {

          dispatch(naturalCardsRequest(response));

        }
      })
      .catch((error) => {
        dispatch(ajaxDataFailed(error));
      })
  }
}