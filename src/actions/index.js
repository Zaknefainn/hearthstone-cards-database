import axios from 'axios';

export const AJAX_DATA_FAILED = 'AJAX_DATA_FAILED';
export const FETCH_CLASS = 'FETCH_CLASS';
export const LIST_CARDS = 'LIST_CARDS';
export const COUNTER_CARDS = 'COUNTER_CARDS';
export const FILTERS_SELECTED = 'FILTERS_SELECTED';

export function ajaxDataFailed(error) {

  return {
    type: AJAX_DATA_FAILED,
    payload: error
  }
}

export function requestHeroClass(response) {

  return {
    type: FETCH_CLASS,
    payload: response.data
  }
}

export function requestListCards(response) {

  return {
    type: LIST_CARDS,
    payload: response.data
  }
}

export const getAllClass = () => {
  
  return (dispatch) => {

    axios.get('http://localhost:3004/class')
      .then((response)=>{

        dispatch(requestHeroClass(response));
      })
      .catch((error) => {

        dispatch(ajaxDataFailed(error));
      })
  }
}

export const getListCards = (parametr = "PALADIN") => {

  return (dispatch) => {

    axios.get(`http://localhost:3004/cards?cardClass=${parametr}`)
      .then((response) => {

        dispatch(requestListCards(response));
      })
      .catch((error) => {
        
        dispatch(ajaxDataFailed(error));
      });
  }
}

export function counterCardsAction(cardCounter) {

  return {
    type: COUNTER_CARDS,
    payload: cardCounter
  }
}

export function filtersSelectedAction(filters) {

  return {
    type: FILTERS_SELECTED,
    payload: filters
  }
}