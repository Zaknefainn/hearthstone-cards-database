import * as actions from '../actions';

const initFilters = {
    classSelected: "PALADIN", //default class
    typeSelected: "ALL",
    raritySelected: "ALL",
    mechanicsSelected: "ALL",
    costSelected: "ALL",
    setSelected: "ALL",
    raceSelected: "ALL"
}

export default (state = initFilters, action) => {

  switch(action.type) {
    case actions.FILTERS_SELECTED:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
}