import * as actions from '../actions';

export default (state = [], action) => {
  switch(action.type) {
    case actions.LIST_CARDS:
      return action.payload;
    default:
      return state;
  }
}