import * as actions from '../actions';

export default function(state = false, action) {

  switch(action.type) {
    case actions.AJAX_DATA_FAILED:
      return {state, isLoading: false, isError: true};
    default:
      return state;
  }
}