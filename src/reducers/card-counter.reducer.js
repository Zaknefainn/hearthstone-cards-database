import * as Actions from '../actions';

export default (state = 0, action) => {

  switch(action.type) {
    case Actions.COUNTER_CARDS:
      return action.payload;
    default:
      return state;
  }
}