import * as actions from '../../actions/class-cards.actions';

export default (state = [], action) => {
  switch(action.type) {
    case actions.DRUID_CLASS:
      return action.payload;
    default:
      return state;
  }
}