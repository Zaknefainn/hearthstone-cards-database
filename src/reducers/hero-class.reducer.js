import * as actions from '../actions';

export default (state = [], action) => {

  switch(action.type) {
    case actions.FETCH_CLASS:
      return [...state, ...action.payload]
    default: 
      return state;
  }
}