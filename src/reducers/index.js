import {combineReducers} from 'redux';
import HeroClassReducer from './hero-class.reducer';
import ErrorReducer from './error.reducer';
import CardListReducer from './card-list.reducer';
import PaladinReducer from './cards/paladin.reducer';
import DruidReducer from './cards/druid.reducer';
import HunterReducer from './cards/hunter.reducer';
import MageReducer from './cards/mage.reducer';
import PriestReducer from './cards/priest.reducer';
import RogueReducer from './cards/rogue.reducer';
import ShamanReducer from './cards/shaman.reducer';
import WarlockReducer from './cards/warlock.reducer';
import WarriorReducer from './cards/warlock.reducer';
import NeutralReducer from './cards/neutral.reducer';
import CardCounterReducer from './card-counter.reducer';
import FilterReducer from './filter.reducer';

const rootReducer = combineReducers({
  heroClass: HeroClassReducer,
  isError: ErrorReducer,
  cardList: CardListReducer,
  paladinCards: PaladinReducer,
  druidCards: DruidReducer,
  hunterCards: HunterReducer,
  mageCards: MageReducer,
  priestCards: PriestReducer,
  rogueCards: RogueReducer,
  shamanCards: ShamanReducer,
  warlockCards: WarlockReducer,
  warriorCards: WarriorReducer,
  narutalCards: NeutralReducer,
  cardCounter: CardCounterReducer,
  filterSelected: FilterReducer
});

export default rootReducer;